# Maintainer : Philip Müller <philm@manjaro.org>

pkgname=umtp-responder
pkgver=1.6.2
pkgrel=1
pkgdesc="Lightweight USB Media Transfer Protocol (MTP) responder daemon"
arch=('aarch64')
url="https://github.com/viveris/uMTP-Responder"
license=('GPL3')
source=("$url/archive/refs/tags/umtprd-$pkgver.tar.gz"
        "$pkgname.service"
        'umtprd.conf')
sha256sums=('1de40511c1dd4618719cff2058dfe68a595f1b9284c80afa89d6d1a1c80aec29'
            '8e7b0286e0691400828b880f603e0ddda171fb5bba24dc6027d85141284e860e'
            'a4a2af2a9663ec0a09c0fa46634cc1ac6f974f05e100f782ccb9ad7f85d1edb5')

prepare() {
  cd "uMTP-Responder-umtprd-$pkgver"

  # apply phone patches
  local src
  for src in "${source[@]}"; do
    src="${src%%::*}"
    src="${src##*/}"
    [[ ${src} = *.patch ]] || continue
    echo "Applying patch ${src}..."
    patch -Np1 < "../${src}"
  done
}

build() {
  cd "uMTP-Responder-umtprd-$pkgver"
  make
}

package() {
  cd "uMTP-Responder-umtprd-$pkgver"
  install -Dm755 umtprd -t "$pkgdir/usr/bin/"
  install -Dm755 conf/umtprd-ffs.sh "$pkgdir/usr/bin/umtprd-ffs"
  install -Dm644 ../umtp-responder.service -t "$pkgdir/usr/lib/systemd/system/"
  install -Dm644 ../umtprd.conf -t "$pkgdir/etc/umtprd/"
}   
